require('dotenv').config()
import express, {Application} from 'express'
import posts from './routes/posts'
import categories from './routes/categories'
import {connectDB} from './utils/db'

const app:Application = express()
connectDB()
app.use(express.json());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE")
    next();
});

app.listen(process.env.APP_PORT, () => console.log('server is running in port', process.env.APP_PORT))

app.use('/posts', posts)
app.use('/categories', categories)

export default app
