import {Request, Response} from 'express'
import Validations from '../validations/categories'
import CategoriesService  from '../services/categories'
import catchAsync from '../utils/catchAsync'

const createCategory = catchAsync(async (req: Request, res: Response) => {
    const {error} = await Validations.createCategory(req.body)
    if(error) return res.status(403).json(error)
    const created_cat: any = await CategoriesService.createCategory(req.body)
    res.json({message: 'Category created', category_id: created_cat.insertId})
 })

const deleteCategory = catchAsync( async (req: Request, res: Response) => {
    const {error} = await Validations.deleteCategory(req.params)
    if(error) return res.status(403).json(error)
    await CategoriesService.deleteCategory(req.params.category_id)
    res.json({message: 'Category deleted'})
})


const updateCategory = catchAsync( async (req: Request, res: Response) => {
    const {error} = await Validations.updateCategory(req.params, req.body)
    if(error) return res.status(403).json(error)
    await CategoriesService.updateCategory(req.params.category_id, req.body)
    res.json({message: 'Category Updated'})
})

const getAllCategories = catchAsync( async (req: Request, res: Response) => {
    const {error} = await Validations.deleteCategory(req.params)
    if(error) return res.status(403).json(error)
    const categories = await CategoriesService.getAllCategories()
    res.json({message: 'All categories', categories})
})

export {createCategory, deleteCategory, updateCategory, getAllCategories}
