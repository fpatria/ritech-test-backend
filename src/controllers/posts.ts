import {Request, Response} from 'express'
import Validations from '../validations/posts'
import PostsService  from '../services/posts'
import catchAsync from '../utils/catchAsync'

const createPost = catchAsync( async (req: Request, res: Response) => {
    const {error} = await Validations.createPost(req.body)
    if(error) return res.status(403).json(error)
    const created_post: any = await PostsService.createPost(req.body)
    res.json({message: 'post created', post_id: created_post.insertId})
})

const deletePost = catchAsync( async (req: Request, res: Response) => {
    const {error} = await Validations.deletePost(req.params)
    if(error) return res.status(403).json(error)
    await PostsService.deletePost(req.params.post_id)
    res.json({message: 'Post Deleted'})
})

const updatePost = async (req: Request, res: Response) => {
    const {error} = await Validations.updatePost(req.params, req.body)
    if(error) return res.status(403).json(error)
    await PostsService.updatePost(req.params.post_id, req.body)
    res.json({message: 'Post Updated'})
}

const getPost = catchAsync( async (req: Request, res: Response) => {
    const {error} = await Validations.getPost(req.params)
    if(error) return res.status(403).json(error)
    const post: any = await PostsService.getOnePost(req.params.post_id)
    res.json({message: 'Single post', post: post.length > 0 ? post[0] : null})
})

const getAllPosts = catchAsync( async (req: Request, res: Response) => {
    res.json({message: 'All posts', posts: await PostsService.getAllPost()})
})

export {createPost, deletePost, updatePost, getPost, getAllPosts}
