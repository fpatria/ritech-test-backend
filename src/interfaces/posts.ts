
interface Post {
    title: string;
    content: string;
    categories: Array<number>;
}

export {Post}
