import express from 'express'
import {createCategory, deleteCategory, updateCategory, getAllCategories} from '../controllers/categories'

const router = express.Router();
 
router.post('/', createCategory);
router.get('/', getAllCategories)
router.delete('/:category_id', deleteCategory)
router.put('/:category_id', updateCategory)

export default router

