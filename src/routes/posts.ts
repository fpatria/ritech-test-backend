import express from 'express'
import {createPost, deletePost, updatePost, getPost, getAllPosts} from '../controllers/posts'

const router = express.Router();
 
router.post('/', createPost);
router.get('/:post_id', getPost)
router.get('/', getAllPosts)
router.delete('/:post_id', deletePost)
router.put('/:post_id', updatePost)


export default router


