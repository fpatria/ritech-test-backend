
import {mysqlQuery} from '../utils/db'
import {Category} from '../interfaces/category'

const createCategory = async (data: Category) =>  {
    const query = `INSERT INTO categories(name) values('${data.name}')`
    return mysqlQuery(query)
}

const deleteCategory = async (cat_id: string) => {
    const query = `DELETE FROM categories WHERE id = ${cat_id}`
    return mysqlQuery(query)
}

const getAllCategories = async () => mysqlQuery('SELECT * FROM categories')

const updateCategory = async (category_id: string, data: Category) => {
    const query = `UPDATE categories set name='${data.name}' WHERE id = ${category_id}`
    return mysqlQuery(query)
}

export = {createCategory, deleteCategory, getAllCategories, updateCategory}