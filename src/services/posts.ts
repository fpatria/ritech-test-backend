
import {mysqlQuery} from '../utils/db'
import {Post} from '../interfaces/posts'

const createPost = async (data: Post) =>  {
    const query = `INSERT INTO posts(title, content,categories) values('${data.title}','${data.content}', '${data.categories.join(',')}')`
    return mysqlQuery(query)
}

const deletePost = async (post_id: string) => {
    const query = `DELETE FROM posts WHERE id = ${post_id}`
    return mysqlQuery(query)
}

const getAllPost = async () => mysqlQuery('SELECT * FROM posts ORDER BY id desc')

const getOnePost = async (post_id: string) => mysqlQuery(`SELECT * FROM posts WHERE id = '${post_id}'`)

const updatePost = async (post_id: string, data: Post) => {
    const query = `UPDATE posts set title='${data.title}', content='${data.content}', categories = '${data.categories}' WHERE id = ${post_id}`
    return mysqlQuery(query)
}

export = {createPost, deletePost, getAllPost, getOnePost, updatePost}