import {Request, Response, NextFunction} from 'express'

interface CallbackFuncion {
   ( req: Request, res: Response, next: NextFunction): void
}

const catchAsync =  (callback: CallbackFuncion )=> async (req: Request,res: Response, next: NextFunction) => {
    Promise.resolve(callback(req, res, next))
    .catch(err => res.status(404).send(err))
} 

export default catchAsync