import mysql, {MysqlError, OkPacket} from 'mysql'

const mysqlConnection = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_SELECTED_DB,
    multipleStatements: true,
})

const connectDB = () => mysqlConnection.connect((error: MysqlError, response: OkPacket) => {
    if(error) {
        console.error('error while connecting to mysql database, please check db config')
        process.exit()
    }
    else console.log('connected to mysql')
})

const mysqlQuery = (query: string) => new Promise((resolve, reject) => {
    mysqlConnection.query(query, (error: MysqlError, data: OkPacket) => error ? reject(error) : resolve(data) )
})

export {mysqlConnection, mysqlQuery, connectDB}