import Joi from "joi";
import {Category} from '../interfaces/category'

const createCategory = (data: Category) => {
    const schema = Joi.object({
        name: Joi.string()
            .min(1)
            .max(255)
            .required(),
    })
    return schema.validate(data);     
}

const updateCategory = (params: object, data: Category) => {
    const schema = Joi.object({
        category_id: Joi.number()
    })
    const validateParams = schema.validate(params); 
    const validateCategory = createCategory(data)
    return validateParams.error ? validateParams : validateCategory
}

const deleteCategory = (data: object) => {
    const schema = Joi.object({
        category_id: Joi.number()
    })
    return schema.validate(data);
}

export default {createCategory, deleteCategory, updateCategory}