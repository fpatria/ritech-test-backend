import Joi from "joi";
import {Post} from '../interfaces/posts'

const createPost = (data: Post) => {
    const schema = Joi.object({
        title: Joi.string()
            .min(1)
            .max(255)
            .required(),
        content: Joi.string()
            .min(1)
            .required(),
        categories: Joi.array()
                    .items(Joi.number())
                    .required()
    })
    return schema.validate(data);   
}

const updatePost = (params: object, body: Post) => {
    const schema = Joi.object({
        post_id: Joi.number()
    })
    const validateParams = schema.validate(params);
    const validatePost = createPost(body)
    return validateParams.error ? validateParams : validatePost
}

const getPost = (data: object) => {
    const schema = Joi.object({
        post_id: Joi.number()
    })
    return schema.validate(data);
}

const deletePost = (data: object) => {
    const schema = Joi.object({
        post_id: Joi.number()
    })
    return schema.validate(data);
}

export default {createPost, getPost, deletePost, updatePost}