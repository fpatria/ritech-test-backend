let chai = require('chai')
let chaiHttp = require('chai-http')
let mocha = require('mocha')
var expect = chai.expect;

chai.use(chaiHttp)

describe('/posts', () => {
    it('should return a list of all posts', (done) => {
        chai.request('http://localhost:5000')
        .get('/posts')
        .end((error, response) => {
            console.log(error)
            expect(response).to.have.status(200);
            expect(response.body).to.have.all.keys('message', 'posts')
            expect(response.body.posts).to.be.a('array')
            done()
         })
    })
})